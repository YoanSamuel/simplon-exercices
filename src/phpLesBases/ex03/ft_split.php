<?php

function ft_split($chaine)
{
    $myarray = preg_split("/[\W]+/", trim($chaine), 0, PREG_SPLIT_NO_EMPTY);
    sort($myarray, SORT_NATURAL);

    return $myarray;
}

// ////// 2eme option ////////
// function ft_split($chaine) {
//   if (empty($chaine)) {
//     return [];
//   }
//   $str = preg_replace("/[\s]+/", ' ', trim($chaine));
//   $myarray = explode(' ', $str);
//   sort($myarray,SORT_NATURAL);
//   return $myarray;
// }
