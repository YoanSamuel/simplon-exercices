<?php

if ($_GET['action'] == 'set') {
    if ($_GET['name'] && $_GET['value']) {
        setcookie($_GET['name'], $_GET['value'], time() + (86400 * 30));
    }
} elseif ($_GET['action'] == 'get') {
    $value = $_COOKIE[$_GET['name']];
    if (isset($value)) {
        echo $value . "\n";
    }
} elseif ($_GET['action'] == 'del') {
    setcookie($_GET['name'], '', time() - 3600);
}

// Solution de Yoan
// foreach ($_GET as $key => $value) {
//   if ($key == 'action') {
//       switch ($value) {
//           case 'set':
//               setcookie($_GET['name'], $_GET['value'], time() + 3600);
//               break;
//           case 'get':
//               if (isset($_COOKIE[$_GET['name']])) {
//                   echo $_COOKIE[$_GET['name']] . "\n";
//               }
//               break;
//           case 'del':
//               setcookie($_GET['name'], $_GET['value'], time() - 3600);
//               break;
//       }
//   }
// }
