<?php

if ($argc !== 4) {
    echo "Incorrect Parameters\n";
    exit;
}

$epur_array = [];

foreach (array_slice($argv, 1) as $value) {
    $epur = preg_replace("/\s+/", ' ', trim($value));
    $epur_array[] = $epur;
}

$number1 = $epur_array[0];
$number2 = $epur_array[2];
$op = $epur_array[1];

if (!is_numeric($number1) || !is_numeric($number2)) {
    echo "Incorrect Parameters\n";
    exit;
}

switch ($epur_array[1]) {
  case '+':
    echo $number1 + $number2 . "\n";
    break;
  case '-':
    echo $number1 - $number2 . "\n";
    break;
  case '*':
    echo $number1 * $number2 . "\n";
    break;
  case '/':
    if (($number1 || $number2) == 0) {
        echo 0 . "\n";
    } else {
        echo $number1 / $number2 . "\n";
    }
    break;
  case '%':
    if (($number1 || $number2) == 0) {
        echo 0 . "\n";
    } else {
        echo $number1 % $number2 . "\n";
    }
    break;
  default:
    echo "Incorrect Parameters\n";
    break;
}
