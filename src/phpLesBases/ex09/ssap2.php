<?php

$numeric_array = $alpha_array = $other_array = [];

$split = trim(implode(' ', array_slice($argv, 1)));
$array = preg_split("/\s/", $split);

foreach ($array as $value) {
    if (is_numeric($value)) {
        $numeric_array[] = $value;
    } elseif (ctype_alpha($value)) {
        $alpha_array[] = $value;
    } else {
        $other_array[] = $value;
    }
}
sort($numeric_array, SORT_STRING);
natcasesort($alpha_array);
sort($other_array);

// $output = array_merge($alpha_array, $numeric_array, $other_array);
$output = [...$alpha_array, ...$numeric_array, ...$other_array];
echo implode("\n", $output) . "\n";

// Ma solution ne fonctionne pas quand on "12983 AAA"
// $newtab = [];
// foreach (array_slice($argv, 1) as $value) {
//     $value = preg_replace("/\s+/", ' ', trim($value));
//     if (str_word_count($value) > 1) {
//       // print_r($value);
//         $array = explode(' ', $value);
//         $newtab = array_merge($newtab, $array);
//     } else {
//         $newtab[] = $value;
//     }
// }
